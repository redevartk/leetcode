class TimeConvert {
    static timeConvert(num: number): string {
        const hours: number = Math.floor(num / 60);
        const minutes: number = num % 60;
        return `${hours}:${minutes}`;
    }
}

// Example usage
const input: number = 120; // 2 hours
console.log("Result: "+(TimeConvert.timeConvert(input)));
document.body.innerHTML = `<p>Result: ${TimeConvert.timeConvert(input)}</p><br>`;