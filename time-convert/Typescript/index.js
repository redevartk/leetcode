var TimeConvert = /** @class */ (function () {
    function TimeConvert() {
    }
    TimeConvert.timeConvert = function (num) {
        var hours = Math.floor(num / 60);
        var minutes = num % 60;
        return "".concat(hours, ":").concat(minutes);
    };
    return TimeConvert;
}());
// Example usage
var input = 120; // 2 hours
console.log("Result: " + (TimeConvert.timeConvert(input)));
document.body.innerHTML = "<p>Result: ".concat(TimeConvert.timeConvert(input), "</p><br>");
