<?php

function timeConvert($num) {
    $hours = intval($num / 60);
    $minutes = $num % 60;
    return $hours . ":" . $minutes;
}

// in minutes
$input = 120;  // 2 hours
// output
echo timeConvert($input);

?>