import java.util.Scanner;

public class TimeConvert {

    public static String timeConvert(int num) {
        int hours = num / 60;
        int minutes = num % 60;
        return hours + ":" + minutes;
    }

    public static void main(String[] args) {
        //in minutes
        int input = 120; // 2 hours
        //output
        System.out.print(timeConvert(input));
    }

}