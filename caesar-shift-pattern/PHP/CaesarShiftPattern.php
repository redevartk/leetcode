<?php

class CaesarShiftPattern {
    private $shiftPattern;

    public function __construct($shiftPattern) {
        $this->shiftPattern = $shiftPattern;
    }

	//complicated formula here
    public function encrypt($plaintext) {
        $letters = range('A', 'Z');
        $input = strtoupper($plaintext);
        $arrInput = str_split($input);
        $result = '';
        $shiftIndex = 0;

        foreach ($arrInput as $char) {
            if (ctype_alpha($char)) {
                $tmp = array_search($char, $letters);
                $shift = $this->shiftPattern[$shiftIndex];
                
                if ($shift >= 0) {
                    $result .= $letters[($tmp + $shift) % 26];
                } else {
                    $result .= $letters[(26 + $tmp + $shift) % 26];
                }

                $shiftIndex = ($shiftIndex + 1) % count($this->shiftPattern);
            } else {
                $result .= $char;
            }
        }

        return $result;
    }

	public function decrypt($ciphertext) {
        $letters = range('A', 'Z');
        $input = strtoupper($ciphertext);
        $arrInput = str_split($input);
        $result = '';
        $shiftIndex = 0;

        foreach ($arrInput as $char) {
            if (ctype_alpha($char)) {
                $charIndex = array_search($char, $letters);
                $shift = $this->shiftPattern[$shiftIndex];

                if ($shift >= 0) {
                    $result .= $letters[(26 + $charIndex - $shift) % 26];
                } else {
                    $result .= $letters[($charIndex - $shift) % 26];
                }

                $shiftIndex = ($shiftIndex + 1) % count($this->shiftPattern);
            } else {
                $result .= $char;
            }
        }

        return $result;
    }
}

// Example usage
$plaintext = "DFHKNQ";

// Define the pattern
$shiftPattern = [1, -2, 3, -4, 5, -6];
$cipher = new CaesarShiftPattern($shiftPattern);
$encryptedText = $cipher->encrypt($plaintext);

// Result
echo $encryptedText;

?>
