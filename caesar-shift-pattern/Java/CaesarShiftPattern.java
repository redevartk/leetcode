import java.util.Arrays;

class CaesarShiftPattern {
    private int[] shiftPattern;

    public CaesarShiftPattern(int[] shiftPattern) {
        this.shiftPattern = shiftPattern;
    }

    // complicated formula here
    public String encrypt(String plaintext) {
        char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        String input = plaintext.toUpperCase();
        char[] arrInput = input.toCharArray();
        StringBuilder result = new StringBuilder();
        int shiftIndex = 0;

        for (char c : arrInput) {
            if (Character.isLetter(c)) {
                int tmp = Arrays.binarySearch(letters, c);
                int shift = shiftPattern[shiftIndex];

                if (shift >= 0) {
                    result.append(letters[(tmp + shift) % 26]);
                } else {
                    result.append(letters[(26 + tmp + shift) % 26]);
                }

                shiftIndex = (shiftIndex + 1) % shiftPattern.length;
            } else {
                result.append(c);
            }
        }

        return result.toString();
    }

    public String decrypt(String ciphertext) {
        char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        String input = ciphertext.toUpperCase();
        char[] arrInput = input.toCharArray();
        StringBuilder result = new StringBuilder();
        int shiftIndex = 0;

        for (char c : arrInput) {
            if (Character.isLetter(c)) {
                int charIndex = Arrays.binarySearch(letters, c);
                int shift = shiftPattern[shiftIndex];

                if (shift >= 0) {
                    result.append(letters[(26 + charIndex - shift) % 26]);
                } else {
                    result.append(letters[(charIndex - shift) % 26]);
                }

                shiftIndex = (shiftIndex + 1) % shiftPattern.length;
            } else {
                result.append(c);
            }
        }

        return result.toString();
    }

    // Example usage
    public static void main(String[] args) {
        String plaintext = "DFHKNQ";

        // Define the pattern
        int[] shiftPattern = {1, -2, 3, -4, 5, -6};
        CaesarShiftPattern cipher = new CaesarShiftPattern(shiftPattern);
        String encryptedText = cipher.encrypt(plaintext);

        // Result
        System.out.println(encryptedText);
    }
}
