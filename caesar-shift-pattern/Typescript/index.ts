class CaesarShiftPattern {
    private shiftPattern: number[];

    constructor(shiftPattern: number[]) {
        this.shiftPattern = shiftPattern;
    }

    // complicated formula here
    encrypt(plaintext: string): string {
        const letters: string[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
        const input: string = plaintext.toUpperCase();
        const arrInput: string[] = input.split("");
        let result: string = "";
        let shiftIndex: number = 0;

        for (const c of arrInput) {
            if (c.match(/[a-zA-Z]/)) {
                const tmp: number = letters.indexOf(c);
                const shift: number = this.shiftPattern[shiftIndex];

                if (shift >= 0) {
                    result += letters[(tmp + shift) % 26];
                } else {
                    result += letters[(26 + tmp + shift) % 26];
                }

                shiftIndex = (shiftIndex + 1) % this.shiftPattern.length;
            } else {
                result += c;
            }
        }

        return result;
    }

    decrypt(ciphertext: string): string {
        const letters: string[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
        const input: string = ciphertext.toUpperCase();
        const arrInput: string[] = input.split("");
        let result: string = "";
        let shiftIndex: number = 0;

        for (const c of arrInput) {
            if (c.match(/[a-zA-Z]/)) {
                const charIndex: number = letters.indexOf(c);
                const shift: number = this.shiftPattern[shiftIndex];

                if (shift >= 0) {
                    result += letters[(26 + charIndex - shift) % 26];
                } else {
                    result += letters[(charIndex - shift) % 26];
                }

                shiftIndex = (shiftIndex + 1) % this.shiftPattern.length;
            } else {
                result += c;
            }
        }

        return result;
    }
}

// Example usage
const plaintext: string = "DFHKNQ";

// Define the pattern
const shiftPattern: number[] = [1, -2, 3, -4, 5, -6];
const cipher: CaesarShiftPattern = new CaesarShiftPattern(shiftPattern);
const encryptedText: string = cipher.encrypt(plaintext);

// Result
console.log("Result: "+encryptedText);
document.body.innerHTML = `<p>Result: ${encryptedText}</p><br>`;