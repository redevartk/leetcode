var CaesarShiftPattern = /** @class */ (function () {
    function CaesarShiftPattern(shiftPattern) {
        this.shiftPattern = shiftPattern;
    }
    // complicated formula here
    CaesarShiftPattern.prototype.encrypt = function (plaintext) {
        var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
        var input = plaintext.toUpperCase();
        var arrInput = input.split("");
        var result = "";
        var shiftIndex = 0;
        for (var _i = 0, arrInput_1 = arrInput; _i < arrInput_1.length; _i++) {
            var c = arrInput_1[_i];
            if (c.match(/[a-zA-Z]/)) {
                var tmp = letters.indexOf(c);
                var shift = this.shiftPattern[shiftIndex];
                if (shift >= 0) {
                    result += letters[(tmp + shift) % 26];
                }
                else {
                    result += letters[(26 + tmp + shift) % 26];
                }
                shiftIndex = (shiftIndex + 1) % this.shiftPattern.length;
            }
            else {
                result += c;
            }
        }
        return result;
    };
    CaesarShiftPattern.prototype.decrypt = function (ciphertext) {
        var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
        var input = ciphertext.toUpperCase();
        var arrInput = input.split("");
        var result = "";
        var shiftIndex = 0;
        for (var _i = 0, arrInput_2 = arrInput; _i < arrInput_2.length; _i++) {
            var c = arrInput_2[_i];
            if (c.match(/[a-zA-Z]/)) {
                var charIndex = letters.indexOf(c);
                var shift = this.shiftPattern[shiftIndex];
                if (shift >= 0) {
                    result += letters[(26 + charIndex - shift) % 26];
                }
                else {
                    result += letters[(charIndex - shift) % 26];
                }
                shiftIndex = (shiftIndex + 1) % this.shiftPattern.length;
            }
            else {
                result += c;
            }
        }
        return result;
    };
    return CaesarShiftPattern;
}());
// Example usage
var plaintext = "DFHKNQ";
// Define the pattern
var shiftPattern = [1, -2, 3, -4, 5, -6];
var cipher = new CaesarShiftPattern(shiftPattern);
var encryptedText = cipher.encrypt(plaintext);
// Result
console.log("Result: " + encryptedText);
document.body.innerHTML = "<p>Result : ".concat(encryptedText, "</p><br>");
