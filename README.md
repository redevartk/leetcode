# My Leetcode Collections

Contain list of my leetcode as training.

## How to run ?

* Java 	: run "java `<filename>.java`", it will print output in cmd/console
* PHP		: run "php `<filename>.php`", it will print output in cmd/console
* JS/TS 	: run "`<filename>.html`" by double clicking it, it will run in your browser console and views page

| Name                      | Description                                                                                            | Solutions                                                                                          |
| ------------------------- | ------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------------- |
| Two Sum                   | Finding two numbers in an array that add up to a given target.                                         | [Java](two-sum/Java), [PHP](two-sum/PHP), [JS/TS](two-sum/Typescript)                                       |
| Valid Parentheses         | Determine if given string is valid parentheses or not                                                  | [Java](valid-parentheses/Java), [PHP](valid-parentheses/PHP), [JS/TS](valid-parentheses/Typescript)         |
| Time Convert              | Convert time from minutes to format hours:minutes                                                      | [Java](time-convert/Java), [PHP](time-convert/PHP), [JS/TS](time-convert/Typescript)                        |
| Combination Sum           | Combination Sum: Finding unique combinations in<br />an array where elements sum up to a given target. | [Java](combination-sum/Java), [PHP](combination-sum/PHP), [JS/TS](combination-sum/Typescript)               |
| Caesar Shift Pattern      | Implementation of Caesar Cipher with shift pattern                                                     | [Java](caesar-shift-pattern/Java),[PHP](caesar-shift-pattern/PHP), [JS/TS](caesar-shift-pattern/Typescript) |
| Ngram Generator           | Generate sequence of n adjacent symbols in particular order                                            | [Java](ngram-generator/Java),[PHP](ngram-generator/PHP), [JS/TS](ngram-generator/Typescript)                |
| Table Generator           | Generate a terminal table based on row and column                                                      | Java,[PHP](table-generator/PHP), JS/TS                                                                |
| Count Lower Case          | Count lower case of a word / character                                                                 | [Java](count-lower-case/Java),[PHP](count-lower-case/PHP), [JS/TS](count-lower-case/Typescript)             |
| Calc Avg High Low         | Calculate Average high and low in string                                                               | Java,[PHP](calc-avg-high-low/PHP), JS/TS                                                              |
| Custom Array Substraction | Simple array substraction (number), with custom length                                                 | Java,[PHP](custom-array-substraction/PHP), JS/TS                                                      |
| Djikstra Path Search      | Path search using Djikstra algorithm                                                                   | [Java](dijkstra-path-search/Java),[PHP](dijkstra-path-search/PHP), [JS/TS](dijkstra-path-search/Typescript) |
| Number Slider             | Find slide number with given value on array                                                            | [Java](number-slider/Java),[PHP](number-slider/PHP), [JS/TS](number-slider/Typescript)                      |
| Remaining Color           | Filters and outputs remaining colors from a predefined list                                            | Java,[PHP](remaining-color/PHP), JS/TS                                                                |
| Robot 2D Movemenet        | Calculate pos of 2D based on input directions and movement                                            | Java,[PHP](robot-2d-movement/PHP), JS/TS                                                              |
| Tower of Hanoi            | Tower of Hanoi puzzle, based on how many floors/disks it has                                           | Java,[PHP](tower-of-hanoi/PHP), JS/TS                                                                 |
