<?php

function twoSum($nums, $target) {
    $numIndicesMap = [];

    for ($i = 0; $i < count($nums); $i++) {
        $complement = $target - $nums[$i];

        if (array_key_exists($complement, $numIndicesMap)) {
            $result = [$numIndicesMap[$complement], $i];

            // Display result
            return "Result: [" . $result[0] . ", " . $result[1] . "]";
        }

        $numIndicesMap[$nums[$i]] = $i;
    }

    // If no solution found
    return "No solution found.";
}

// Example usage:
// Change this according to your need
$nums = [5, 5, 9, 9, 1, 1];
$target = 2;

// Call the twoSum function
$result = twoSum($nums, $target);

// Print the result
echo $result . PHP_EOL;
?>