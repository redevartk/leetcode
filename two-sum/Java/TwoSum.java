import java.util.HashMap;
import java.util.Map;

public class TwoSum {

    public static String twoSum(int[] nums, int target) {
        Map<Integer, Integer> numIndicesMap = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            int complement = target - nums[i];

            if (numIndicesMap.containsKey(complement)) {
                int[] result = {numIndicesMap.get(complement), i};

                // Display result
                return "Result: [" + result[0] + ", " + result[1] + "]";
            }

            numIndicesMap.put(nums[i], i);
        }

        // If no solution found
        return "No solution found.";
    }

    //main
    public static void main(String[] args) {
        // Example usage:
        // Change this according to your need
        int[] nums = {5, 5, 9, 9, 1, 1};
        int target = 2;

        // Call the twoSum function
        String result = twoSum(nums, target);

        //print the result
        System.out.println("Output: " + result);
    }
}