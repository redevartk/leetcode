//calculate two sum
function twoSum(nums: number[], target: number): number[] | string {
    const numIndicesMap: Map<number, number> = new Map();

    for (let i = 0; i < nums.length; i++) {
        const complement = target - nums[i];

        if (numIndicesMap.has(complement)) {
            const result = [numIndicesMap.get(complement)!, i];

            //display result in body
            document.body.innerHTML = `<p>Result: [${result}]</p>`;

            // Display no solution message to console
            console.log('Result:', result);

            return result;
        }

        numIndicesMap.set(nums[i], i);
    }

    // Display no solution in console
    console.log('No solution found.');

    // Display no solution in body
    return 'No solution found.';
}

// Example usage:
// Change this according to your need
const nums = [5,5,9,9,1,1];
const target = 2;

//print output
twoSum(nums, target);