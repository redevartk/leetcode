import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ValidParentheses {

    public static boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        char bracketPlaceHolder = '#';

        // formula of bracket, or what valid is
        Map<Character, Character> bracketMap = new HashMap<>();
        bracketMap.put(')', '(');
        bracketMap.put('}', '{');
        bracketMap.put(']', '[');

        for (int i = 0; i < s.length(); i++) {
            char currentBracket = s.charAt(i);

            // for closing bracket
            if (bracketMap.containsKey(currentBracket)) {
                char topElement = stack.isEmpty() ? bracketPlaceHolder : stack.pop();
                if (bracketMap.get(currentBracket) != topElement) {
                    return false;
                }

            // for opening bracket
            } else {
                stack.push(currentBracket);
            }
        }

        // if the stack is empty, then all brackets are matched
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        // display result in console

        // true
        System.out.println(isValid("()"));

        // false
        System.out.println(isValid("(]"));

        // true
        System.out.println(isValid("{[]}"));

        // false
        System.out.println(isValid("([)]"));

        // true
        System.out.println(isValid("()[]{}"));
    }
}