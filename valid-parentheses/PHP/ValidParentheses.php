<?php

function isValid($s) {
    $stack = new SplStack();
    $bracketPlaceHolder = '#';

    // formula of bracket, or what valid is
    $bracketMap = array(
        ')' => '(',
        '}' => '{',
        ']' => '['
    );

    for ($i = 0; $i < strlen($s); $i++) {
        $currentBracket = $s[$i];

        // for closing bracket
        if (array_key_exists($currentBracket, $bracketMap)) {
            $topElement = $stack->isEmpty() ? $bracketPlaceHolder : $stack->pop();
            if ($bracketMap[$currentBracket] != $topElement) {
                return false;
            }

        // for opening bracket
        } else {
            $stack->push($currentBracket);
        }
    }

    // if the stack is empty, then all brackets are matched
    return $stack->isEmpty();
}

// display result in console

// true
var_dump(isValid("()"));

// false
var_dump(isValid("(]"));

// true
var_dump(isValid("{[]}"));

// false
var_dump(isValid("([)]"));

// true
var_dump(isValid("()[]{}"));
?>