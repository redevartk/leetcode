function validParentheses(s: string): boolean {
    const stack: string[] = [];
    const bracketPlaceHolder = '#';

    //formula of bracket, or what valid is
    const bracketMap: { [key: string]: string } = {
        ')': '(',
        '}': '{',
        ']': '['
    };

    for (let i = 0; i < s.length; i++) {
        const currentBracket = s[i];

        //for closing bracket
        if (bracketMap[currentBracket]) {
            const topElement = stack.pop() || bracketPlaceHolder;
            if (bracketMap[currentBracket] !== topElement) {
                return false;
            }

        //for opening bracket
        } else {
            stack.push(currentBracket);
        }
    }

    // if the stack is empty, then all brackets are matched
    return stack.length === 0;
}

//display result in console

//true
console.log(validParentheses("()"));

//false
console.log(validParentheses("(]"));     

//true
console.log(validParentheses("{[]}"));     

//false
console.log(validParentheses("([)]"));    

//true
console.log(validParentheses("()[]{}"));

//display result in body, result is same as  above
document.body.innerHTML = `<p>Result for () : ${validParentheses("()")}</p><br>`;
document.body.innerHTML += `<p>Result for (] : ${validParentheses("(]")}</p><br>`;
document.body.innerHTML += `<p>Result for {[]} : ${validParentheses("{[]}")}</p><br>`;
document.body.innerHTML += `<p>Result for ([)] : ${validParentheses("([)]")}</p><br>`;
document.body.innerHTML += `<p>Result for ()[]{} : ${validParentheses("()[]{}")}</p><br>`;