<?php

class DijkstraPathSearch
{
    private $graph;

    public function __construct()
    {
        $this->graph = [];
    }

    //add an edge with its weight to the graph
    public function addEdge($startVertex, $endVertex, $weight)
    {
        $this->graph[$startVertex][$endVertex] = $weight;
    }

    //dijkstra algorithm
    public function dijkstra($start, $end)
    {
        $distances = [];
        $previousVertices = [];

        $priorityQueue = new SplPriorityQueue();

        //initialize distances and insert vertices into the priority queue
        foreach ($this->graph as $vertex => $adjacent) {
            $distances[$vertex] = PHP_INT_MAX;
            $previousVertices[$vertex] = null;
            foreach ($adjacent as $neighbor => $edgeWeight) {
                $priorityQueue->insert($neighbor, -$edgeWeight);
            }
        }

        $distances[$start] = 0;

        //process vertices until the priority queue is empty
        while (!$priorityQueue->isEmpty()) {
            $currentVertex = $priorityQueue->extract();

            if (!isset($this->graph[$currentVertex])) {
                continue;
            }

            foreach ($this->graph[$currentVertex] as $neighbor => $edgeWeight) {
                $alternativeDistance = $distances[$currentVertex] + $edgeWeight;

                if ($alternativeDistance < $distances[$neighbor]) {
                    $distances[$neighbor] = $alternativeDistance;
                    $previousVertices[$neighbor] = $currentVertex;
                }
            }
        }

        $shortestPath = [];
        $currentVertex = $end;

        while (isset($previousVertices[$currentVertex])) {
            array_unshift($shortestPath, $currentVertex);
            $currentVertex = $previousVertices[$currentVertex];
        }

        array_unshift($shortestPath, $start);

        return ['path' => $shortestPath, 'distance' => $distances[$end]];
    }

    //calculate and display the shortest path and distance
    public function calculatePath($start, $end)
    {
        //call algorithm
        $result = $this->dijkstra($start, $end);

        //output
        echo "Path Detail: " . implode(', ', $result['path']) . "\n";
        echo "Distance Total: " . $result['distance'] . "\n";
    }
}

//example usage
$dijkstraPathSearch = new DijkstraPathSearch();

//add edges and weights to the graph
$dijkstraPathSearch->addEdge('A', 'B', 5);
$dijkstraPathSearch->addEdge('B', 'C', 2);
$dijkstraPathSearch->addEdge('C', 'F', 1);
$dijkstraPathSearch->addEdge('F', 'G', 4);
$dijkstraPathSearch->addEdge('G', 'E', 2);
$dijkstraPathSearch->addEdge('E', 'D', 3);
$dijkstraPathSearch->addEdge('D', 'F', 7);
$dijkstraPathSearch->addEdge('C', 'D', 2);
$dijkstraPathSearch->addEdge('D', 'B', 10);
$dijkstraPathSearch->addEdge('D', 'A', 6);

//calculate and display the shortest path and distance
//result
$dijkstraPathSearch->calculatePath('A', 'C');
