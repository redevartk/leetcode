class DijkstraPathSearch {
    private graph: { [key: string]: { [key: string]: number } };

    constructor() {
        this.graph = {};
    }

    // add an edge with its weight to the graph
    public addEdge(startVertex: string, endVertex: string, weight: number): void {
        if (!this.graph[startVertex]) {
            this.graph[startVertex] = {};
        }
        this.graph[startVertex][endVertex] = weight;
    }

    // dijkstra algorithm
    public dijkstra(start: string, end: string): { path: string[], distance: number } {
        const distances: { [key: string]: number } = {};
        const previousVertices: { [key: string]: string | null } = {};
        const priorityQueue: string[] = [];

        // initialize distances and insert vertices into the priority queue
        for (const vertex in this.graph) {
            distances[vertex] = Number.POSITIVE_INFINITY;
            previousVertices[vertex] = null;
            for (const neighbor in this.graph[vertex]) {
                priorityQueue.push(neighbor);
            }
        }

        distances[start] = 0;

        // process vertices until the priority queue is empty
        while (priorityQueue.length > 0) {
            priorityQueue.sort((a, b) => distances[a] - distances[b]);
            const currentVertex = priorityQueue.shift();

            if (!currentVertex || !this.graph[currentVertex]) {
                continue;
            }

            for (const neighbor in this.graph[currentVertex]) {
                const edgeWeight = this.graph[currentVertex][neighbor];
                const alternativeDistance = distances[currentVertex] + edgeWeight;
                if (alternativeDistance < (distances[neighbor] ?? Number.POSITIVE_INFINITY)) {
                    distances[neighbor] = alternativeDistance;
                    previousVertices[neighbor] = currentVertex;
                }
            }
        }

        // construct shortest path
        const shortestPath: string[] = [];
        let currentVertex: string | null = end;

        while (previousVertices[currentVertex] !== null && currentVertex) {
            shortestPath.unshift(currentVertex);
            currentVertex = previousVertices[currentVertex];
        }

        shortestPath.unshift(start);

        return { path: shortestPath, distance: distances[end] };
    }

    // calculate and display the shortest path and distance
    public calculatePath(start: string, end: string): void {
        // call algorithm
        const result = this.dijkstra(start, end);
        // output
        console.log("Path Detail: " + result.path.join(', '));
        console.log("Distance Total: " + result.distance);

        document.body.innerHTML = "Path Detail: " + result.path.join(', ') +"<br>";
        document.body.innerHTML += "Distance Total: " + result.distance;
    }
}

// example usage
const dijkstraPathSearch = new DijkstraPathSearch();
// add edges and weights to the graph
dijkstraPathSearch.addEdge('A', 'B', 5);
dijkstraPathSearch.addEdge('B', 'C', 2);
dijkstraPathSearch.addEdge('C', 'F', 1);
dijkstraPathSearch.addEdge('F', 'G', 4);
dijkstraPathSearch.addEdge('G', 'E', 2);
dijkstraPathSearch.addEdge('E', 'D', 3);
dijkstraPathSearch.addEdge('D', 'F', 7);
dijkstraPathSearch.addEdge('C', 'D', 2);
dijkstraPathSearch.addEdge('D', 'B', 10);
dijkstraPathSearch.addEdge('D', 'A', 6);

// calculate and display the shortest path and distance
// result
dijkstraPathSearch.calculatePath('A', 'C');
