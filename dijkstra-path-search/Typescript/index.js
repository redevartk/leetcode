var DijkstraPathSearch = /** @class */ (function () {
    function DijkstraPathSearch() {
        this.graph = {};
    }
    // add an edge with its weight to the graph
    DijkstraPathSearch.prototype.addEdge = function (startVertex, endVertex, weight) {
        if (!this.graph[startVertex]) {
            this.graph[startVertex] = {};
        }
        this.graph[startVertex][endVertex] = weight;
    };
    // dijkstra algorithm
    DijkstraPathSearch.prototype.dijkstra = function (start, end) {
        var _a;
        var distances = {};
        var previousVertices = {};
        var priorityQueue = [];
        // initialize distances and insert vertices into the priority queue
        for (var vertex in this.graph) {
            distances[vertex] = Number.POSITIVE_INFINITY;
            previousVertices[vertex] = null;
            for (var neighbor in this.graph[vertex]) {
                priorityQueue.push(neighbor);
            }
        }
        distances[start] = 0;
        // process vertices until the priority queue is empty
        while (priorityQueue.length > 0) {
            priorityQueue.sort(function (a, b) { return distances[a] - distances[b]; });
            var currentVertex_1 = priorityQueue.shift();
            if (!currentVertex_1 || !this.graph[currentVertex_1]) {
                continue;
            }
            for (var neighbor in this.graph[currentVertex_1]) {
                var edgeWeight = this.graph[currentVertex_1][neighbor];
                var alternativeDistance = distances[currentVertex_1] + edgeWeight;
                if (alternativeDistance < ((_a = distances[neighbor]) !== null && _a !== void 0 ? _a : Number.POSITIVE_INFINITY)) {
                    distances[neighbor] = alternativeDistance;
                    previousVertices[neighbor] = currentVertex_1;
                }
            }
        }
        // construct shortest path
        var shortestPath = [];
        var currentVertex = end;
        while (previousVertices[currentVertex] !== null && currentVertex) {
            shortestPath.unshift(currentVertex);
            currentVertex = previousVertices[currentVertex];
        }
        shortestPath.unshift(start);
        return { path: shortestPath, distance: distances[end] };
    };
    // calculate and display the shortest path and distance
    DijkstraPathSearch.prototype.calculatePath = function (start, end) {
        // call algorithm
        var result = this.dijkstra(start, end);
        // output
        console.log("Path Detail: " + result.path.join(', '));
        console.log("Distance Total: " + result.distance);
        document.body.innerHTML = "Path Detail: " + result.path.join(', ') + "<br>";
        document.body.innerHTML += "Distance Total: " + result.distance;
    };
    return DijkstraPathSearch;
}());
// example usage
var dijkstraPathSearch = new DijkstraPathSearch();
// add edges and weights to the graph
dijkstraPathSearch.addEdge('A', 'B', 5);
dijkstraPathSearch.addEdge('B', 'C', 2);
dijkstraPathSearch.addEdge('C', 'F', 1);
dijkstraPathSearch.addEdge('F', 'G', 4);
dijkstraPathSearch.addEdge('G', 'E', 2);
dijkstraPathSearch.addEdge('E', 'D', 3);
dijkstraPathSearch.addEdge('D', 'F', 7);
dijkstraPathSearch.addEdge('C', 'D', 2);
dijkstraPathSearch.addEdge('D', 'B', 10);
dijkstraPathSearch.addEdge('D', 'A', 6);
// calculate and display the shortest path and distance
// result
dijkstraPathSearch.calculatePath('A', 'B');
