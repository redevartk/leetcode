var NgramGenerator = /** @class */ (function () {
    function NgramGenerator(input) {
        this.inputString = input;
    }
    NgramGenerator.prototype.generateUnigrams = function () {
        return this.inputString.toLowerCase().split(' ');
    };
    //generate bigrams
    NgramGenerator.prototype.generateBigrams = function () {
        var words = this.inputString.toLowerCase().split(' ');
        var bigrams = [];
        for (var i = 0; i < words.length - 1; i += 2) {
            var bigram = "".concat(words[i], " ").concat(words[i + 1]);
            bigrams.push(bigram);
        }
        return bigrams;
    };
    //generate trigrams
    NgramGenerator.prototype.generateTrigrams = function () {
        var words = this.inputString.toLowerCase().split(' ');
        var trigrams = [];
        for (var i = 0; i < words.length - 2; i += 3) {
            var trigram = "".concat(words[i], " ").concat(words[i + 1], " ").concat(words[i + 2]);
            trigrams.push(trigram);
        }
        return trigrams;
    };
    return NgramGenerator;
}());
//example usage
var inputString = "Jakarta adalah ibukota negara Republik Indonesia";
var ngramGenerator = new NgramGenerator(inputString);
//generate and display unigrams
var unigrams = ngramGenerator.generateUnigrams();
console.log("Unigram: " + unigrams.join(', '));
document.body.innerHTML = "Unigram: " + unigrams.join(', ') + "<br>";
//generate and display bigrams
var bigrams = ngramGenerator.generateBigrams();
console.log("Bigram: " + bigrams.join(', '));
document.body.innerHTML += "Bigram: " + bigrams.join(', ') + "<br>";
//generate and display trigrams
var trigrams = ngramGenerator.generateTrigrams();
console.log("Trigram: " + trigrams.join(', '));
document.body.innerHTML += "Trigram: " + trigrams.join(', ');
