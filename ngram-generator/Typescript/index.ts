class NgramGenerator {
    private inputString: string;

    constructor(input: string) {
        this.inputString = input;
    }

    generateUnigrams(): string[] {
        return this.inputString.toLowerCase().split(' ');
    }

    //generate bigrams
    generateBigrams(): string[] {
        const words: string[] = this.inputString.toLowerCase().split(' ');
        const bigrams: string[] = [];

        for (let i = 0; i < words.length - 1; i += 2) {
            const bigram: string = `${words[i]} ${words[i + 1]}`;
            bigrams.push(bigram);
        }

        return bigrams;
    }

    //generate trigrams
    generateTrigrams(): string[] {
        const words: string[] = this.inputString.toLowerCase().split(' ');
        const trigrams: string[] = [];

        for (let i = 0; i < words.length - 2; i += 3) {
            const trigram: string = `${words[i]} ${words[i + 1]} ${words[i + 2]}`;
            trigrams.push(trigram);
        }

        return trigrams;
    }
}

//example usage
const inputString: string = "Jakarta adalah ibukota negara Republik Indonesia";
const ngramGenerator: NgramGenerator = new NgramGenerator(inputString);

//generate and display unigrams
const unigrams: string[] = ngramGenerator.generateUnigrams();
console.log("Unigram: " + unigrams.join(', '));
document.body.innerHTML = "Unigram: " + unigrams.join(', ')+"<br>";

//generate and display bigrams
const bigrams: string[] = ngramGenerator.generateBigrams();
console.log("Bigram: " + bigrams.join(', '));
document.body.innerHTML += "Bigram: " + bigrams.join(', ')+"<br>";

//generate and display trigrams
const trigrams: string[] = ngramGenerator.generateTrigrams();
console.log("Trigram: " + trigrams.join(', '));
document.body.innerHTML += "Trigram: " + trigrams.join(', ');
