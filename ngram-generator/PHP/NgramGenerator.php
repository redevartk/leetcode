<?php

class NgramGenerator {
    
    private $inputString;

    public function __construct($input) {
        $this->inputString = $input;
    }

    public function generateUnigrams() {
        return explode(' ', strtolower($this->inputString));
    }

    //manipulated non-repeated bigram
    public function generateBigrams() {
        $words = explode(' ', strtolower($this->inputString));
        $bigrams = array();

        for ($i = 0; $i < count($words) - 1; $i += 2) {
            $bigram = $words[$i].' '.$words[$i + 1];
            $bigrams[] = $bigram;
        }

        return $bigrams;
    }

    //manipulated non-repeated trigram
    public function generateTrigrams() {
        $words = explode(' ', strtolower($this->inputString));
        $trigrams = array();

        for ($i = 0; $i < count($words) - 2; $i += 3) {
            $trigram = $words[$i].' '.$words[$i + 1].' '.$words[$i + 2];
            $trigrams[] = $trigram;
        }

        return $trigrams;
    }
}

//example usage
$inputString = "Jakarta adalah ibukota negara Republik Indonesia";
$ngramGenerator = new NgramGenerator($inputString);

//generate and display unigrams
$unigrams = $ngramGenerator->generateUnigrams();
echo "Unigram: " . implode(', ', $unigrams) . PHP_EOL;

//generate and display bigrams
$bigrams = $ngramGenerator->generateBigrams();
echo "Bigram: " . implode(', ', $bigrams) . PHP_EOL;

//generate and display trigrams
$trigrams = $ngramGenerator->generateTrigrams();
echo "Trigram: " . implode(', ', $trigrams) . PHP_EOL;

?>
