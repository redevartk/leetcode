<?php

class CountLowerCase {
    
    private $inputString;

    public function __construct($input) {
        $this->inputString = $input;
    }

    public function countLowerCase() {
        $lowercaseCount = 0;

        //loop through each character in the string
        for ($i = 0; $i < strlen($this->inputString); $i++) {
            //check if the character is a lowercase letter
            if (ctype_lower($this->inputString[$i])) {
                $lowercaseCount++;
            }
        }

        return $lowercaseCount;
    }
}

//example usage
$inputString = "TranSISI";
$countLowerCaseLetters = new CountLowerCase($inputString);
$lowercaseCount = $countLowerCaseLetters->countLowerCase();

//results
echo "\"{$inputString}\" have {$lowercaseCount} lowercase." . PHP_EOL;

?>