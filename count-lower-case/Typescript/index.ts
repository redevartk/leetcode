class CountLowerCase {
    private inputString: string;

    constructor(input: string) {
        this.inputString = input;
    }

    countLowerCase(): number {
        let lowercaseCount: number = 0;

        //loop through each character in the string
        for (let i = 0; i < this.inputString.length; i++) {
            //check if the character is a lowercase letter
            if (this.inputString[i] === this.inputString[i].toLowerCase()) {
                lowercaseCount++;
            }
        }

        return lowercaseCount;
    }
}

//example usage
const inputString: string = "TranSISI";
const countLowerCaseLetters: CountLowerCase = new CountLowerCase(inputString);
const lowercaseCount: number = countLowerCaseLetters.countLowerCase();

//results
console.log(`"${inputString}" have ${lowercaseCount} lowercase.`);
document.body.innerHTML = `"${inputString}" have ${lowercaseCount} lowercase.`;
