var CountLowerCase = /** @class */ (function () {
    function CountLowerCase(input) {
        this.inputString = input;
    }
    CountLowerCase.prototype.countLowerCase = function () {
        var lowercaseCount = 0;
        //loop through each character in the string
        for (var i = 0; i < this.inputString.length; i++) {
            //check if the character is a lowercase letter
            if (this.inputString[i] === this.inputString[i].toLowerCase()) {
                lowercaseCount++;
            }
        }
        return lowercaseCount;
    };
    return CountLowerCase;
}());
//example usage
var inputString = "TranSISI";
var countLowerCaseLetters = new CountLowerCase(inputString);
var lowercaseCount = countLowerCaseLetters.countLowerCase();
//results
console.log("\"".concat(inputString, "\" have ").concat(lowercaseCount, " lowercase."));
document.body.innerHTML = "\"".concat(inputString, "\" have ").concat(lowercaseCount, " lowercase.");
