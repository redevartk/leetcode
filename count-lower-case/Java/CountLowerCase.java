public class CountLowerCase {
    private String inputString;

    public CountLowerCase(String input) {
        this.inputString = input;
    }

    public int countLowerCase() {
        int lowercaseCount = 0;

        //loop through each character in the string
        for (int i = 0; i < inputString.length(); i++) {
            //check if the character is a lowercase letter
            if (Character.isLowerCase(inputString.charAt(i))) {
                lowercaseCount++;
            }
        }

        return lowercaseCount;
    }

    public static void main(String[] args) {
        //example usage
        String inputString = "TranSISI";
        CountLowerCase countLowerCaseLetters = new CountLowerCase(inputString);
        int lowercaseCount = countLowerCaseLetters.countLowerCase();

        //results
        System.out.println("\"" + inputString + "\" have " + lowercaseCount + " lowercase.");
    }
}
