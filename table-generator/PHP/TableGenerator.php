<?php

class TableGenerator {

    private $rowCount;
    private $colCount;

    public function __construct($rowCount, $colCount) {
        $this->rowCount = $rowCount;
        $this->colCount = $colCount;
    }

    public function generateTable() {
        $counter = 1;

        for ($i = 1; $i <= $this->rowCount; $i++) {
            for ($j = 1; $j <= $this->colCount; $j++) {
                echo str_pad($counter, 5, ' ', STR_PAD_LEFT);
                $counter++;
            }
            echo PHP_EOL;
        }
    }
}

// Example usage
$tableGenerator = new TableGenerator(8, 8); //row and column

// Result
$tableGenerator->generateTable();
