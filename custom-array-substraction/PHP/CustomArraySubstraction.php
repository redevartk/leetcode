<?php

class ArraySubtractionCalculator
{
    private $numbers;
    private $length;

    public function __construct($arrayLength, $numbers)
    {
        $this->numbers = $numbers;
        //use specific array length if provided
        if (!empty($arrayLength)) {
            $this->length = min($arrayLength, count($numbers));
        } else {
            $this->length = count($numbers);
        }        
    }

    public function calculateSubtraction()
    {
        $result = null;

        for ($i = 0; $i < $this->length; $i++) {
            $number = $this->numbers[$i];

            if ($result === null) {
                $result = $number;
            } else {
                $result -= $number;
            }
        }

        return $result;
    }
}

//create an instance of the class
$subtractionCalculator = new ArraySubtractionCalculator(3, [10, 2, 1]);

//example usage
$outputLength = $subtractionCalculator->calculateSubtraction();

//result
echo $outputLength;
