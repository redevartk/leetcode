<?php

class Robot2DMovement
{
    private $x;
    private $y;
    private $facing;

    public function __construct()
    {
        //initialize position
        $this->x = 0;
        $this->y = 0;
        $this->facing = 'north'; //default facing or directions
    }

    public function calculateFinalPosition($directions, $movements)
    {
        //define forward and backward position
        $movementTranslations = array('forward' => 1, 'backward' => -1);

        if (!empty($directions)) {
            //loop to check right and left for facing
            for ($i = 0; $i < count($directions); $i++) {
                switch (strtolower($directions[$i])) {
                    case 'right':
                        $this->facing = [
                            'north' => 'east',
                            'east' => 'south',
                            'south' => 'west',
                            'west' => 'north'
                        ][$this->facing];
                        break;
                    case 'left':
                        $this->facing = [
                            'north' => 'west',
                            'west' => 'south',
                            'south' => 'east',
                            'east' => 'north'
                        ][$this->facing];
                        break;
                }
            }
        }

        //get final position according to facing
        for ($i = 0; $i < count($movements); $i++) {
            $movementFilter = strtolower($movements[$i]);

            //update position according to movement translation and formula given
            $this->y += $movementTranslations[$movementFilter] * ($this->facing == 'north' ? 1 : ($this->facing == 'south' ? -1 : 0));
            $this->x += $movementTranslations[$movementFilter] * ($this->facing == 'east' ? 1 : ($this->facing == 'west' ? -1 : 0));
        }

        return array($this->x, $this->y);
    }
}

//create an instance of the class
$robot = new Robot2DMovement();

//example usage
$inputDirections = ['right', 'right'];
$inputMovements = ['forward', 'forward', 'forward'];
$output = $robot->calculateFinalPosition($inputDirections, $inputMovements);

//result
echo "(" . implode(",", $output) . ")\n";