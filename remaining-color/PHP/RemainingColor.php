<?php

class RemainingColor{

    private $headColor;

    public function __construct($headColor) {
        $this->headColor = $headColor;
    }

    function filterRainbowColorFromHead() {
        //color list
        $rainbowColors = [
            're' => 'red',
            'or' => 'orange',
            'ye' => 'yellow',
            'gr' => 'green',
            'bl' => 'blue',
            'in' => 'indigo',
            'pu' => 'purple'
        ];
    
        //check if colors exist
        foreach ($this->headColor as $color) {
            if (!array_key_exists($color, $rainbowColors)) {
                return "Invalid color input, pls use color shortened input.";
            }
        }
    
        //filter the array, 
        //use array key for output instead of value for this case
        $remainingColors = array_diff(array_keys($rainbowColors), $this->headColor);
    
        //generate string output
        return implode(' ', $remainingColors);
    }
}

//example usage
$headColor = ['re', 'or'];
$remainingColor = new RemainingColor($headColor);
$result = $remainingColor->filterRainbowColorFromHead();

//result
echo $result;

