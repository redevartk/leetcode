class CombinationSum {
    combinationSumFunc(candidates: number[], target: number): number[][] {
        const result: number[][] = [];
        //sort candidates to handle duplicates
        candidates.sort((a, b) => a - b);

        //call backtrack function
        this.backtrack(result, [], candidates, target, 0);
        return result;
    }

    //recursive function to check all possible combinations
    private backtrack(result: number[][], current: number[], candidates: number[], target: number, start: number): void {
        //stop when target = 0
        if (target === 0) {
            result.push([...current]);
            return;
        }

        const length = candidates.length;
        for (let i = start; i < length; i++) {
            //skip duplicates
            if (i > start && candidates[i] === candidates[i - 1]) {
                continue;
            }

            //skip if candidate > target
            if (candidates[i] > target) {
                continue;
            }

            //check combination for the current candidate
            current.push(candidates[i]);

            //recall backtrack (recursive function)
            this.backtrack(result, current, candidates, target - candidates[i], i + 1);

            //remove current (backtrack)
            current.pop();
        }
    }
}

//create new combination sum
const solution = new CombinationSum();

//static input
const candidates: number[] = [2, 5, 2, 1, 2];
const target: number = 5;

//get result
const result: number[][] = solution.combinationSumFunc(candidates, target);

//show output
console.log("Output: " + JSON.stringify(result));
document.body.innerHTML = "Output: " + JSON.stringify(result);