var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var CombinationSum = /** @class */ (function () {
    function CombinationSum() {
    }
    CombinationSum.prototype.combinationSumFunc = function (candidates, target) {
        var result = [];
        // sort candidates to handle duplicates
        candidates.sort(function (a, b) { return a - b; });
        // call backtrack function
        this.backtrack(result, [], candidates, target, 0);
        return result;
    };
    // recursive function to check all possible combinations
    CombinationSum.prototype.backtrack = function (result, current, candidates, target, start) {
        // stop when target = 0
        if (target === 0) {
            result.push(__spreadArray([], current, true));
            return;
        }
        var length = candidates.length;
        for (var i = start; i < length; i++) {
            // skip duplicates
            if (i > start && candidates[i] === candidates[i - 1]) {
                continue;
            }
            // skip if candidate > target
            if (candidates[i] > target) {
                continue;
            }
            // check combination for the current candidate
            current.push(candidates[i]);
            // recall backtrack (recursive function)
            this.backtrack(result, current, candidates, target - candidates[i], i + 1);
            // remove current (backtrack)
            current.pop();
        }
    };
    return CombinationSum;
}());
// create new combination sum
var solution = new CombinationSum();
// static input
var candidates = [2, 5, 2, 1, 2];
var target = 5;
// get result
var result = solution.combinationSumFunc(candidates, target);
// show output
console.log("Output: " + JSON.stringify(result));
document.body.innerHTML = "Output: " + JSON.stringify(result);
