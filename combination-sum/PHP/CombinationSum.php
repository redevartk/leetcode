<?php

class CombinationSum {
    public function combinationSumFunc($candidates, $target) {
        $result = [];
        //sort candidates to handle duplicates
        sort($candidates);

        //call backtrack function
        $this->backtrack($result, [], $candidates, $target, 0);
        return $result;
    }

    //recursive function to check all possible combinations
    private function backtrack(&$result, $current, $candidates, $target, $start) {
        //stop when target = 0
        if ($target == 0) {
            $result[] = $current;
            return;
        }

        $length = count($candidates);
        for ($i = $start; $i < $length; $i++) {
            //skip duplicates
            if ($i > $start && $candidates[$i] == $candidates[$i - 1]) {
                continue;
            }

            //skip if candidate > target
            if ($candidates[$i] > $target) {
                continue;
            }

            //check combination for the current candidate
            $current[] = $candidates[$i];

            //recall backtrack (recursive function)
            $this->backtrack($result, $current, $candidates, $target - $candidates[$i], $i + 1);

            //remove current (backtrack)
            array_pop($current);
        }
    }
}

//create new combination sum
$solution = new CombinationSum();

//static input
$candidates = [2, 5, 2, 1, 2];
$target = 5;

//get result
$result = $solution->combinationSumFunc($candidates, $target);

//show output
echo "Output: ".json_encode($result).PHP_EOL;

?>