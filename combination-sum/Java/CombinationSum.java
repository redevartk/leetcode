import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CombinationSum {
    public List<List<Integer>> combinationSumFunc(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        //sort canditate from duplicate
        Arrays.sort(candidates);

        //call backtrack func
        backtrack(result, new ArrayList<>(), candidates, target, 0);
        return result;
    }

    //recursive function to check all possible combination
    private void backtrack(List<List<Integer>> result, List<Integer> current, int[] candidates, int target, int start) {
        //stop until target = 0
        if (target == 0) {
            result.add(new ArrayList<>(current));
            return;
        }

        for (int i = start; i < candidates.length; i++) {
            //skip duplicate
            if (i > start && candidates[i] == candidates[i - 1]) {
                continue;
            }

            //skip if candidate > target
            if (candidates[i] > target) {
                continue;
            }

            //check combination for current candidate
            current.add(candidates[i]);

            //recall backtrack (recursive function)
            backtrack(result, current, candidates, target - candidates[i], i + 1);

            //remove current
            current.remove(current.size() - 1); // Backtrack
        }
    }

    public static void main(String[] args) {
        //create new combination sum
        CombinationSum solution = new CombinationSum();

        //static input
        int[] candidates = {2, 5, 2, 1, 2};
        int target = 5;

        //get result
        List<List<Integer>> result = solution.combinationSumFunc(candidates, target);

        //show output
        System.out.println("Output: " + result);
    }
}