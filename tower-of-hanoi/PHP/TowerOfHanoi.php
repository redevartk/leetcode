<?php

class TowerOfHanoi
{
    private $towerPoles = array(array(), array(), array());
    private $stepCounter = 1;

    public function solve($numberOfDisks, $sourcePole = "A", $auxiliaryPole = "B", $destinationPole = "C")
    {
        //check number of disk
        if ($numberOfDisks > 0) {
            //move n-1 disks from source to auxiliary pole using destination as the auxiliary pole
            $this->solve($numberOfDisks - 1, $sourcePole, $destinationPole, $auxiliaryPole);

            echo "Step {$this->stepCounter} :\n";
            $this->stepCounter++;
            $this->moveDisk($sourcePole, $destinationPole);
            $this->displayPoles();

            //move the n-1 disks from auxiliary pole to destination using source as the auxiliary pole
            $this->solve($numberOfDisks - 1, $auxiliaryPole, $sourcePole, $destinationPole);
        } else {
            return; //do nothing if no disk
        }
    }

    private function initializePoles($numberOfDisks)
    {
        //place n disks on the first pole (source pole)
        for ($i = $numberOfDisks; $i >= 1; --$i) {
            $this->towerPoles[0][] = $i;
        }
    }

    private function moveDisk($sourcePole, $destinationPole)
    {
        //determine the indices of source and destination poles
        if ($sourcePole == "A") {
            $sourcePoleIndex = 0;
        } elseif ($sourcePole == "B") {
            $sourcePoleIndex = 1;
        } else {
            $sourcePoleIndex = 2;
        }
        
        if ($destinationPole == "A") {
            $destinationPoleIndex = 0;
        } elseif ($destinationPole == "B") {
            $destinationPoleIndex = 1;
        } else {
            $destinationPoleIndex = 2;
        }
        

        //pop the top disk from the source pole and push it onto the destination pole
        $topDisk = array_pop($this->towerPoles[$sourcePoleIndex]);
        array_push($this->towerPoles[$destinationPoleIndex], $topDisk);
    }

    private function displayPoles()
    {
        echo "[" . implode(", ", $this->towerPoles[0]) . "] ";
        echo "[" . implode(", ", $this->towerPoles[1]) . "] ";
        echo "[" . implode(", ", $this->towerPoles[2]) . "] ";
        echo "\n\n";
    }

    public function getResult($numberOfDisks)
    {

        //initialize
        if($numberOfDisks < 3){
            return "floor/disk cannot go below 3";
        }
        $this->initializePoles($numberOfDisks);

        echo "Left is highest, right is lowest \n";
        echo "tower [A][B][C]\n";
        $this->displayPoles();

        $this->solve($numberOfDisks);
    }
}

//example usage
$numberOfDisks = 3; //floor/disk
$towerOfHanoi = new TowerOfHanoi();

//result
$towerOfHanoi->getResult($numberOfDisks);
