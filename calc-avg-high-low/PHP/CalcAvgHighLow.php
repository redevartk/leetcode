<?php

class CalcAvgHighLow {
    
    private $nilaiArr;

    public function __construct($data) {
        // Convert to array
        $this->nilaiArr = array_map('intval', explode(' ', $data));
    }

    public function calculateAverage() {
        return array_sum($this->nilaiArr) / count($this->nilaiArr);
    }

    public function getHighestValues($data) {
        $sortedArray = $this->nilaiArr;
        rsort($sortedArray);
        return array_slice($sortedArray, 0, $data);
    }

    public function getLowestValues($data) {
        $sortedArray = $this->nilaiArr;
        sort($sortedArray);
        return array_slice($sortedArray, 0, $data);
    }
}

// Example usage
$nilai = "72 65 73 78 75 74 90 81 87 65 55 69 72 78 79 91 100 40 67 77 86";

$calcAvgHighLow = new CalcAvgHighLow($nilai);

$average = $calcAvgHighLow->calculateAverage();
$highest_values = $calcAvgHighLow->getHighestValues(7);
$lowest_values = $calcAvgHighLow->getLowestValues(7);

// Results
echo "Nilai Rata-rata: " . $average . PHP_EOL;
echo "7 Nilai Tertinggi: " . implode(", ", $highest_values) . PHP_EOL;
echo "7 Nilai Terendah: " . implode(", ", $lowest_values) . PHP_EOL;

?>