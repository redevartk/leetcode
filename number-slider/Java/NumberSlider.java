import java.util.Arrays;
import java.util.List;

class NumberSlider {
    private List<Integer> numberSequence;
    private int startIndex;

    public NumberSlider(Integer[] numberSequence, Integer startValue) {
        this.numberSequence = Arrays.asList(numberSequence);
        if (startValue != null) {
            this.startIndex = this.numberSequence.indexOf(startValue);
        } else {
            this.startIndex = 0;
        }
    }

    public int slide(int movingInput) {
        int adjustedIndex = movingInput + this.startIndex;

        if (adjustedIndex >= 0 && adjustedIndex < this.numberSequence.size()) {
            return this.numberSequence.get(adjustedIndex);
        }

        return this.numberSequence.get(this.numberSequence.size() - 1);
    }

    public static void main(String[] args) {
        // create an instance of the class
        NumberSlider numberSlider = new NumberSlider(new Integer[]{0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50}, 25);

        // example usage
        int movingInput = 3;
        int result = numberSlider.slide(movingInput);

        // result
        System.out.println(result);
    }
}
