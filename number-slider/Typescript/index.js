var NumberSlider = /** @class */ (function () {
    function NumberSlider(numberSequence, startValue) {
        if (startValue === void 0) { startValue = null; }
        this.numberSequence = numberSequence;
        if (startValue !== null) {
            this.startIndex = this.numberSequence.indexOf(startValue);
        }
        else {
            this.startIndex = 0;
        }
    }
    NumberSlider.prototype.slide = function (movingInput) {
        var adjustedIndex = movingInput + this.startIndex;
        if (adjustedIndex >= 0 && adjustedIndex < this.numberSequence.length) {
            return this.numberSequence[adjustedIndex];
        }
        return this.numberSequence[this.numberSequence.length - 1];
    };
    return NumberSlider;
}());
//create an instance of the class
var numberSlider = new NumberSlider([0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50], 25);
//example usage
var movingInput = 3;
var result = numberSlider.slide(movingInput);
//result
console.log(result);
document.body.innerHTML = result.toString();
