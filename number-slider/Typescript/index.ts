class NumberSlider {
    private numberSequence: number[];
    private startIndex: number;

    constructor(numberSequence: number[], startValue: number | null = null) {
        this.numberSequence = numberSequence;
        if (startValue !== null) {
            this.startIndex = this.numberSequence.indexOf(startValue);
        } else {
            this.startIndex = 0;
        }
    }

    public slide(movingInput: number): number {
        const adjustedIndex = movingInput + this.startIndex;

        if (adjustedIndex >= 0 && adjustedIndex < this.numberSequence.length) {
            return this.numberSequence[adjustedIndex];
        }

        return this.numberSequence[this.numberSequence.length - 1];
    }
}

//create an instance of the class
const numberSlider = new NumberSlider([0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50], 25);

//example usage
const movingInput = 3;
const result = numberSlider.slide(movingInput);

//result
console.log(result);
document.body.innerHTML = result.toString();
