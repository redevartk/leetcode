<?php

class NumberSlider
{
    private $numberSequence;
    private $startIndex;

    public function __construct($numberSequence, $startValue = null)
    {
        $this->numberSequence = $numberSequence;
        if ($startValue !== null) {
            $this->startIndex = array_search($startValue, $numberSequence);
        } else {
            $this->startIndex = 0;
        }        
    }

    public function slide($movingInput)
    {
        $adjustedIndex = $movingInput + $this->startIndex;

        if ($adjustedIndex >= 0 && $adjustedIndex < count($this->numberSequence)) {
            return $this->numberSequence[$adjustedIndex];
        }

        return end($this->numberSequence);
    }
}

//create an instance of the class
$numberSlider = new NumberSlider([0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50], 25);

//example usage
$movingInput = 3;
$result = $numberSlider->slide($movingInput);

//result
echo $result;